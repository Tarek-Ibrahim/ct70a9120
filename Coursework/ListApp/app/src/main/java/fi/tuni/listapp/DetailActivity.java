package fi.tuni.listapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.view.Display;
import android.widget.ImageView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent in = getIntent();
        int index = in.getIntExtra("fi.tuni.ITEM_INDEX",-1);

        if (index > -1){
            int pic = getImg(index);
            ImageView img = (ImageView) findViewById(R.id.imageView);
            scaleImg(img,pic);
        }
    }

    //For readability and modularity purposes, we divide the functionality and processing of the data among two additional methods
    //first, get the image corresponding to the received index
    private int getImg (int index){
        switch (index) {
            case 0: return R.drawable.peach; //peach = name of corresp. image in drawable folder
            case 1: return R.drawable.tomato;
            case 2: return R.drawable.squash;
            default: return -1;
        }
    }

    //second, scale the image (bc/ too big images can crash the emulator app)
    private void scaleImg (ImageView img, int pic) { //pic=the image we want to scale
        Display screen = getWindowManager().getDefaultDisplay(); //instance of the device screen
        BitmapFactory.Options options = new BitmapFactory.Options(); //instance of a java library for image scaling

        options.inJustDecodeBounds = true; //turn on the boundaries for the BitmapFactory instance
        BitmapFactory.decodeResource(getResources(), pic, options); //access the "pic" without actually drawing it (which saves us memory)
        //apply "options" to the "pic" (since the boundaries of the "options" are turned on it obtains the dimensions of the "pic" after being applied to it)

        int imgWidth = options.outWidth; //outermost width of the image "pic"
        int screenWidth = screen.getWidth();

        if (imgWidth>screenWidth){ //condition for applying scaling
            int ratio = Math.round((float)imgWidth/(float)screenWidth);
            options.inSampleSize=ratio; //set scaling factor
        }
        options.inJustDecodeBounds=false;
        Bitmap scaledImg = BitmapFactory.decodeResource(getResources(),pic,options); //apply "options" to the "pic" (scaling it with the set scale factor)
        img.setImageBitmap(scaledImg);
    }

}