package fi.tuni.listapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView myListView; //create a class attribute variable of ListView DT
    String[] items; //a string array [DT]
    String[] prices;
    String[] descriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources(); //just for easiness and readability //create a reference to the project's resources
        myListView= (ListView) findViewById(R.id.MyListView);
        items=res.getStringArray(R.array.items); //look in our project's resources (i.e. res folder) for an object with a stringArray DT among the arrays with name "items" in our resources
        //we had already inserted the items in the values [folder] > strings.xml [file] as a String array
        prices=res.getStringArray(R.array.prices);
        descriptions=res.getStringArray(R.array.descriptions);

        //In case of only operating on items stringArray/list
        //Now we have 2 lists: a list (array) of strings and its correspoding list of View
        //To present these 2 lists together combined we do so using a Layout
        //So, first we create a new layout file in the Layout folder of the project (with root element = TextView) [called: my_listview_detail]
        //Then, we use an adapter to combine the two lists using the created layout
        //myListView.setAdapter(new ArrayAdapter<String>(this,R.layout.my_listview_detail,items)); //for the ListView object we create an adapter of type ArrayAdapter (using the object's setAdapter method) //The ArrayAdapter is of type String (bc/ we want to adapt the ListView to a string array) //"this" keyword refers to the current object/context (i.e. the ListView)
        //ArrayAdapter(ListView,Layout that will combine them, The String Array)

        //In case of operating on all 3 stringArrays
        //Now we have 4 lists: ListView + the 3 string arrays
        //To create an adapter to merge the lists together using a layout, we will do it in a separate [class] file for modularity and readability of code, then invoke it here after it's implemented
        //To create that class file, right-click on the <app domain name> folder (i.e. the Java main package in the project) and select New>Java Class
        ItemAdapter ItemAdapter = new ItemAdapter(this, items, prices, descriptions); //create the adapter object (as a reference)
        myListView.setAdapter(ItemAdapter); //for the ListView object we create an adapter of the custom type ItemAdapter (using the object's setAdapter method)

        //Displaying a picture of every item in the list in a new activity when clicked
        //First create a new activity (DetailActivity) and its xml insert an image placeholder (imageview>drag>MipMap>ic_lancher)
        //Here, we employ a similar idea: one placeholder gets modified according to the item that is being operated on
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent showDetailActivity = new Intent(getApplicationContext(),DetailActivity.class);
                showDetailActivity.putExtra("fi.tuni.ITEM_INDEX",i); //we switch to the picture screen with the item index, so that the corresponding activity can pull the right picture (the picture corresponding to the passed index) to occupy the placeholder
                startActivity(showDetailActivity);
            }
        });
    }
}