package fi.tuni.listapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter { // add: extends BaseAdapter
    //hover over the above line and click "Implement Methods" (to implement the class' abstract methods) to autocomplete the necessary methods

    //first [re-]declare the class' attributes, which it's going to be manipulating/working on
    LayoutInflater mInflater; //m = [class] member //needed to put the stringArrays in the created layout xml file, to then put it in the ListView
    // The inflater is needed because we will make a list of items as per the defined String Arrays in the layout described in the xml file (the layout contains only a template for one item but the inflater will inflate it to the size of the string array list to apply it to all the items and view the result)
    String[] items; // this is like self.items (and later self.items=i, where i = items from the mainActivity)
    String[] prices;
    String[] descriptions;

    public ItemAdapter(Context c, String[] i, String[] p, String[] d) { //constructor //context = self / this (refers to the object)
        items=i;
        prices=p;
        descriptions=d;
        mInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) { //Id =index
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) { //how to present the info.
        // i is the current item [index] in the list that is being worked on

        View v= mInflater.inflate(R.layout.my_listview_detail,null); //inflate the inflater variable with the layout "my_listview_detail"

        TextView nameTextView = (TextView) v.findViewById(R.id.nameTextView); //v.findViewById() looks specifically within/inside the view "v" which is the inflated layout "my_listview_detail"
        TextView priceTextView = (TextView) v.findViewById(R.id.priceTextView); // using v. is also essential because it should be defined wrt the inflater
        TextView descriptionTextView = (TextView) v.findViewById(R.id.descriptionTextView);

        String name = items[i];
        String desc= descriptions[i];
        String cost= prices[i];

        nameTextView.setText(name);
        priceTextView.setText(cost);
        descriptionTextView.setText(desc);

        return v;
    }
}
