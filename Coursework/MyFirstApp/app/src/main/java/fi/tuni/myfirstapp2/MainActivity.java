package fi.tuni.myfirstapp2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addBtn = (Button) findViewById(R.id.AddBtn); //obtain the value of the Add button in our app //create a variable "AddBtn" of Button datatype and assign it a value (which for saftey is also cast to Button DT) which is obtained from the corresponding button in our app
        // R=resources; R.id.<id_name> = Search in resources for an [item with an] id that matches <id_name> (i.e. from resources, find by id, the object with <id_name>)
        addBtn.setOnClickListener(new View.OnClickListener() {  //Like a subscriber and a callback function for every time the button is clicked during the app runtime
            @Override
            public void onClick(View view) { //Put in this function the events/actions you would like to happen whenever the button is clicked
                EditText firstNumEditText = (EditText) findViewById(R.id.firstNumEditText);
                EditText secondNumEditText = (EditText) findViewById(R.id.secondNumEditText);
                TextView resultTextView = (TextView) findViewById(R.id.ResultTextView); //ties ResultTextView to resultTextView (creates a REFERENCE variable)

                int num1 = Integer.parseInt(firstNumEditText.getText().toString()); // parse [the string] for int DT //extract the integer number from the EditText DT stored in firstNumEditText variable (EditText -> .getText() -> Text -> .toString() -> String -> Integer.parseInt() -> int)
                int num2 = Integer.parseInt(secondNumEditText.getText().toString());
                int result= num1 + num2;

                resultTextView.setText(result + ""); // convert int number in result to String by concatenating with empty string "" // result -> resultTextView -> ResultTextView
            }
        });
    }
}