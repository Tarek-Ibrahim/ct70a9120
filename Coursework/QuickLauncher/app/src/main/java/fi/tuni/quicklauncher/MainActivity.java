package fi.tuni.quicklauncher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //SecondActivity Button (launch another activity WITHIN our app)
        Button secondActivityBtn = (Button) findViewById(R.id.secondActivityBtn);
        secondActivityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(getApplicationContext(), SecondActivity.class); //create an intent [object] (i.e. an action linked to clicking the corresponding button) which switches the application context (i.e. the screen WITHIN the app) to the SecondActivity [class] in the project
                startIntent.putExtra("fi.tuni.QuickLauncher.SOMETHING","HELLO WORLD!"); //any extra info that you'd like this intent to carry (it's like assigning values to the "extra" attribute of the Intent object) //(key,value) //sends this value to SecondActivity that can be accessed with its correspoding key
                startActivity(startIntent); // a built-in method that takes an intent [object] and starts it
            }
        });

        //Google Button (launch another activity OUTSIDE our app)
        Button googleBtn = (Button) findViewById(R.id.googleBtn);
        googleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String google = "https://www.google.com";
                Uri webaddress = Uri.parse(google); //parse the string for a Uri DT (universal reference identifier)

                Intent goToGoogle = new Intent(Intent.ACTION_VIEW, webaddress); //creates an Intent which sends the webaddress variable value to be broadcasted (as a request) to all the apps on the current device in order for the appropriate app to pick it up as an input to execute it
                if (goToGoogle.resolveActivity(getPackageManager()) != null) { //check if the intent will be resolved by the device's package manager (i.e. if the intent/webaddress, once broadcasted, will be handled by an app on the device
                    startActivity(goToGoogle);
                }
            }
        });

    }
}