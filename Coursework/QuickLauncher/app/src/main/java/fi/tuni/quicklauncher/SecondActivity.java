package fi.tuni.quicklauncher;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (getIntent().hasExtra("fi.tuni.QuickLauncher.SOMETHING")) { //check if this activity is started with the current intent having the extra info of the mainactivity or not (bc it's possible that the app could start with this screen directly)
            TextView tv = (TextView) findViewById(R.id.textView); //reference the textView object on the 2nd screen
            String text = getIntent().getExtras().getString("fi.tuni.QuickLauncher.SOMETHING"); //get the string value using its key and store it in a variable
            tv.setText(text);
        }
    }
}