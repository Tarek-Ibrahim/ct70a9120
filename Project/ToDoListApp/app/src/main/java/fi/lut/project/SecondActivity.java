package fi.lut.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;



public class SecondActivity extends AppCompatActivity {

    EditText titleEditText;
    ArrayList<String> list;

//    SharedPreferences sharedPref = SecondActivity.this.getPreferences(Context.MODE_PRIVATE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        titleEditText = (EditText) findViewById(R.id.titleEditText);

        list=getIntent().getStringArrayListExtra("fi.lut.project.LIST");

        Button addListBtn = (Button) findViewById(R.id.addListBtn);
        Button cancelListBtn = (Button) findViewById(R.id.cancelListBtn);

        cancelListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                finish();
                Intent home = new Intent(getApplicationContext(),MainActivity.class);
                home.putExtra("fi.lut.project.CANCEL",0);
                home.putStringArrayListExtra("fi.lut.project.LIST",list);
                startActivity(home);
            }
        });

        addListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title =titleEditText.getText().toString();
                if(title.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter a task",Toast.LENGTH_LONG).show();
                }
                else {
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString("fi.lut.project.TASK",title);
//                    editor.commit();
//                    finish();
                    Intent home = new Intent(getApplicationContext(), MainActivity.class);
                    home.putExtra("fi.lut.project.TASK", title);
                    home.putStringArrayListExtra("fi.lut.project.LIST", list);
                    startActivity(home);
                }

            }
        });
    }
}