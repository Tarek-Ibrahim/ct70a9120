package fi.lut.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends BaseAdapter { // add: extends BaseAdapter
    //hover over the above line and click "Implement Methods" (to implement the class' abstract methods) to autocomplete the necessary methods

    //first [re-]declare the class' attributes, which it's going to be manipulating/working on
    LayoutInflater mInflater; //m = [class] member //needed to put the stringArrays in the created layout xml file, to then put it in the ListView
    // The inflater is needed because we will make a list of items as per the defined String Arrays in the layout described in the xml file (the layout contains only a template for one item but the inflater will inflate it to the size of the string array list to apply it to all the items and view the result)
    List<String> title=new ArrayList<String>();
    List<String> desc=new ArrayList<String>();
//    String[] title; // this is like self.items (and later self.items=i, where i = items from the mainActivity)
//    String[] desc;

    public ItemAdapter(Context c, ArrayList<String> t, List<String> d) { //constructor //context = self / this (refers to the object)
        title=t;
        desc=d;
        mInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return 0;
    }

    @Override
    public long getItemId(int i) { //Id =index
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) { //how to present the info.
        // i is the current item [index] in the list that is being worked on

        View v= mInflater.inflate(R.layout.my_listview_detail,null); //inflate the inflater variable with the layout "my_listview_detail"

//        TextView titleTextView = (TextView) v.findViewById(R.id.titleTextView); //v.findViewById() looks specifically within/inside the view "v" which is the inflated layout "my_listview_detail"
//        TextView descTextView = (TextView) v.findViewById(R.id.descTextView);


//        titleTextView.setText(title);
//        descTextView.setText(desc);

        return v;
    }
}
