package fi.lut.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class ThirdActivity extends AppCompatActivity {

    ArrayList<String> titleList=new ArrayList<String>();
    EditText updateEditText;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        updateEditText = (EditText) findViewById(R.id.updateEditText);
        Button updateBtn = (Button) findViewById(R.id.updateBtn);
        Button deleteBtn = (Button) findViewById(R.id.deleteBtn);
        Button backBtn = (Button) findViewById(R.id.backBtn);

        titleList = getIntent().getStringArrayListExtra("fi.lut.project.LIST");
        i = getIntent().getIntExtra("fi.lut.project.ITEM_INDEX",-1);
        String item = titleList.get(i);
        updateEditText.setText(item);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(getApplicationContext(),MainActivity.class);
                home.putExtra("fi.lut.project.CANCEL",0);
                home.putStringArrayListExtra("fi.lut.project.LIST",titleList);
                startActivity(home);
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title =updateEditText.getText().toString();
                if(title.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter a task",Toast.LENGTH_LONG).show();
                }
                else {
                    Intent home = new Intent(getApplicationContext(), MainActivity.class);
                    titleList.set(i,title);
                    home.putExtra("fi.lut.project.CANCEL",0);
                    home.putStringArrayListExtra("fi.lut.project.LIST", titleList);
                    startActivity(home);
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(getApplicationContext(), MainActivity.class);
                titleList.remove(i);
                home.putExtra("fi.lut.project.CANCEL",0);
                home.putStringArrayListExtra("fi.lut.project.LIST", titleList);
                startActivity(home);
            }
        });

    }
}