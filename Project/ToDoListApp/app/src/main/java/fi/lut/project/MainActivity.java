package fi.lut.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> titleList=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView myListView =(ListView) findViewById(R.id.myListView);
        FloatingActionButton addBtn = (FloatingActionButton) findViewById(R.id.addBtn);


        if (getIntent().hasExtra("fi.lut.project.TASK")) {
            String titleText = getIntent().getExtras().getString("fi.lut.project.TASK");
            titleList = getIntent().getStringArrayListExtra("fi.lut.project.LIST");
            titleList.add(titleText);
            myListView.setAdapter(new ArrayAdapter<String>(this,R.layout.my_listview_detail,titleList));

        }

        else if (getIntent().hasExtra("fi.lut.project.CANCEL")){
            titleList = getIntent().getStringArrayListExtra("fi.lut.project.LIST");
            myListView.setAdapter(new ArrayAdapter<String>(this,R.layout.my_listview_detail,titleList));
        }

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(getApplicationContext(),SecondActivity.class);
                startIntent.putStringArrayListExtra("fi.lut.project.LIST",titleList);
                startActivity(startIntent);
            }
        });

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent showThirdActivity = new Intent(getApplicationContext(),ThirdActivity.class);
                showThirdActivity.putStringArrayListExtra("fi.lut.project.LIST",titleList);
                showThirdActivity.putExtra("fi.lut.project.ITEM_INDEX",i);
                startActivity(showThirdActivity);
            }
        });

    }
}