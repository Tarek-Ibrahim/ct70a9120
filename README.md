# CT70A9120_15.04.2020 Software Development Skills: Mobile

**Tarek Ibrahim**

## Directories of the repository:

1. **Coursework**: Contains three directories corresponding to the three apps developed in the tutorial series.
2. **Learning Diary**: Has the learning diary in .doc and .pdf formats.
3. **Project**: Contains the directory of the app developed for the project work part of the course, entitled "ToDoListApp", and a text file which has a link to a Youtube video showcasing the app in action. 

---

## Working with the apps:

In **Android Studio**, from File>Open, select the folder corresponding to the desired app. Make sure Android Studio has a working emulator (virtual device) or a recogized physical android device (an android phone) connected to the computer, then click run.

When using a physical device, first on the device, go to Settings>About phone>Device Information, and tap 7 times on "Build number" to become a developer. Additionaly, enable USB debugger option.